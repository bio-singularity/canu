# Canu singularity recipe

A Singularity recipe to create an image which wrap the Canu assembly tool.

We use the canu software v1.7.1 (latest stable release [Aug 2018]).

## Use it

### Build the image

```bash
	sudo singularity build canu.simg Singularity
```

### Get the image from repositories

Will be available soon on singularity-hub. We are waiting for a compatibility with gitlab.

### launch canu

```bash
	./canu.simg <canu parameters>
```

## Links

* [Canu publication](https://doi.org/10.1101/gr.215087.116)
* [Canu github](https://github.com/marbl/canu)
* [Singularity](https://www.sylabs.io/)
